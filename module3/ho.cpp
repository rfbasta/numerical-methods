#include <iostream>
#include <cmath>
#include "../ran2_double.cpp"

// Warning: Questo codice è quello che succede quando
// qualcuno prova ad imparare il C++ per necessità
// abituato al C. Probabilmente fa schifo ma funziona.


class Path {
	private:
		double *p;
		int size;
	public:
		Path (int);
		~Path();
		double &operator[] (int);
};

double &Path::operator[](int index) {
	// Questa ridefinizione dell'operatore path[i] mi permette
	// di implementare qua dentro le condizioni periodiche
	// Volendo poi potrò provare a passare a un sistema di lookup
	while (index >= size)
		index -= size;
	while (index < 0)
		index += size;
	return p[index];
}

Path::Path(int s = 0) {
	// Constructor dell'oggetto Path
	// Crea un cammino di dimensione s e lo inizializza a 0
	size = s;
	p = nullptr;
	if (s != 0) {
		p = new double[s];
		for (int i = 0; i < s; i++)
			p[i] = 0;
	}
}

Path::~Path() {
	// Distruttore dell'oggetto Path
	// Scritto più per completezza che per necessità
	// Tanto alla fine del programma si dealloca tutto da solo
	delete [] p;
}

class Simulation {
	private:
		int size;	// lunghezza del cammino
		int accepted;	// passi accettati
		int steps;	// passi fatti
		double delta;	// parametro metropolis
		double eta;	// parametro eta
		Path *p;
	public:
		Simulation(double, int, double);
		~Simulation();
		void Metrostep();
		double Acceptance();
		double Measure_x2();
};

double Simulation::Measure_x2() {
	int i;
	double m = 0;
	for (i = 0; i < size; i++)
		m += pow((*p)[i], 2);
	m /= size;
	return m;
}

Simulation::~Simulation() {
	delete p;
}

Simulation::Simulation(double Neta, int N, double d = 0) {
	size = N;
	eta = Neta / N;
	steps = 0;
	accepted = 0;
	p = new Path(N);
	if (d == 0)
		delta = 2 * sqrt(eta);
	else
		delta = d;
}

void Simulation::Metrostep() {
	int i;
	double x, yp, r;
	for (i = 0; i < size; i++) {	// spazza il cammino, rispetta il bilancio dettagliato
		yp = (*p)[i] + delta*(2*ran2() - 1);

		r = exp( - (pow(yp,2) - pow((*p)[i],2))*((eta/2) + (1 / eta)) + (1/eta) * (yp - (*p)[i])*((*p)[i+1] + (*p)[i-1]));

		if (r > 1) {
			accepted++;
			(*p)[i] = yp;
		} else {
			x = ran2();
			if (x < r) {
				accepted++;
				(*p)[i] = yp;
			}
		}
		steps++;
	}
}

double Simulation::Acceptance() {
	double a = accepted;
	a /= steps;
	return a;
}

int main() {
	ran2_init();
	
	std::cout.precision(17); // double precision

	unsigned int n_measures = 1000000;
	unsigned int n_skip = 10;
	unsigned int n_thermal = 100000;
	unsigned int n = 10;
	double neta = 10;


	Simulation sim(neta, n, 0.5);

	unsigned int i,j;

	for (i = 0; i < n_thermal; i++)
		sim.Metrostep();
	std::cout << "#Beginning simulation with Neta = " << neta << ", N = " << n << " \n";
	std::cout << "#y^2\n";

	for (i = 0; i < n_measures; i++) {
		for (j = 0; j < n_skip; j++)
			sim.Metrostep();
			
		std::cout << sim.Measure_x2() << "\n";
	} 
	std::cout << "#Generated " << n_measures << " measures with acceptance: " << sim.Acceptance() << std::endl;
	ran2_save();
	return 0;
}

