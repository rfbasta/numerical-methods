#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "../ran2.c"



// Funzioni di inizializzazione del cammino
double * create_path(unsigned int n) {			// TODO: sarebbe bello farci uno struct per non dover passare continuamente n alle funzioni...
	double * p = malloc(n * sizeof(double));	// magari anche uno struct sui parametri della simulazione...?
	return p;
}

double * destroy_path(double * p) {
	free(p);
	return NULL;
}

void initialize_path(double * p, unsigned int n) {
	unsigned int i;
	// per ora mi limito a inizializzare a zero
	for (i = 0; i < n; i++)
		p[i] = i;
}

// Funzione di debug sul cammino
void print_path(double * p, unsigned int n) {
	unsigned int i;
	for (i = 0; i < n; i++)
		printf("%f\t", p[i]);
	printf("\n");
}

// In C, % non implementa il modulo => workaround
int mod(int a, int b) {
	int r = a % b;
	return r < 0 ? r + b : r;
}

void metropolis(double * p, unsigned int n, double eta, double delta, unsigned int *accepted) {
	unsigned int i,j;
	double x, yp, r;
	for (i = 0; i < n; i++) { //spazza il cammino, rispetta il bilancio dettagliato
		yp = p[i] + delta*(2*ran2() - 1);	// y[i] di prova

		r = exp( - (pow(yp,2) - pow(p[i],2))*((eta/2) + (1 / eta)) + (1/eta) * (yp - p[i])*(p[mod(i+1, n)] + p[mod(i-1, n)]));

		if (r > 1) {
			(*accepted)++;
			p[i] = yp;
		}
		else {
			x = ran2();
			if (x < r) {
				(*accepted)++;
				p[i] = yp;
			}
		}	
	}
}

double x2(double * p, unsigned int n) {
	unsigned int i;
	double m = 0;
	for (i = 0; i < n; i++)
		m += pow(p[i],2);
	m /= n;
	return m;
}

double dx2(double * p , unsigned int n) {
	unsigned int i;
	double m = 0;
	for (i = 0; i < n; i++)
		m += pow((p[i] - p[mod(i-1, n)]), 2);
	m /= n;
	return m;
}


int main(int argc, char *argv[]) {
	if (argc == 1) {
		printf("Syntax is %s [N*eta] [N] \n", argv[0]);
	       	return EXIT_FAILURE;
	}	
	ran2_init();
	//double eta = 0.03;
	double Neta = atof(argv[1]);
	unsigned int l = atoi(argv[2]);
	double eta = Neta / l;
	printf("#Beginning simulation with Neta = %f, l = %d, eta = %f\n", Neta, l, eta);
	unsigned int n_measures = 1000000;
	unsigned int n_skip = 10;
	unsigned int n_thermal = 100000;
	unsigned int accept = 0;
	double delta = 2* sqrt(eta);
	double * path = create_path(l);
	initialize_path(path, l);

	unsigned int i,j;

	for (i = 0; i < n_thermal; i++)
		metropolis(path, l, eta, delta, &accept);

	printf("#y^2\tdy^2\n");
	for (i = 0; i < n_measures; i++) {
		for (j = 0; j < n_skip; j++)
			metropolis(path, l, eta, delta, &accept);
		printf("%f\t%f\n", x2(path, l), dx2(path, l));
	}

	printf("#Acceptance: %d out of %d (%f)\n", accept, n_measures*n_skip*l, ((float) accept) / (n_measures*n_skip*l));
	path = destroy_path(path);
	ran2_save();
	return EXIT_SUCCESS;
}
