#include <iostream>
#include <cmath>
#include "../ran2_double.cpp"

class Paths {
	private:
		double *p0;
		double *p1;
		int size;
		int sigma;
	public:
		Paths(int, int);
		~Paths();
		double &operator() (int, int);
		void flip_sigma();
		int get_sigma();
};

Paths::Paths(int s = 0, int init = 0) {
	// Constructor dell'oggetto Paths
	// Crea due cammini di dimensione s
	size = s;
	sigma = 1;
	p0 = nullptr;
	p1= nullptr;
	if (s != 0) {
		p0 = new double[s];
		p1 = new double[s];
		switch (init) {
			case 0:	for (int i = 0; i < s; i++) {
					p0[i] = 0;
					p1[i] = 0;
				}
				break;
			case 1:	for (int i = 0; i < s; i++) {
					p0[i] = 1 - 2*ran2();
					p1[i] = 1 - 2*ran2();
				}
				break;
			default:	std::cerr << "Illegal init value. Exiting." << std::endl;
					exit(EXIT_FAILURE);
		}
	}
}

Paths::~Paths() {
	// Distruttore dell'oggetto Paths
	// Dealloca tutto
	delete [] p0;
	delete [] p1;
}

int Paths::get_sigma() {
	return sigma;
}

void Paths::flip_sigma() {
	sigma = -sigma;
}

double &Paths::operator()(int path, int index) {
	int pm = 0;
	switch (sigma) {
		case 1: 	while (index >= size)
					index -= size;
				while (index < 0)
					index += size;
				break;
		case -1:	while (index >= size) {
					index -= size;
					pm++;
				}
				while (index < 0) {
					index += size;
					pm++;
				}
				break;
	}
	path += pm;
	while (path > 1) 
		path -= 2;
	while (path < 0)
		path += 2;
	switch (path) {
		case 0:	return p0[index];
		case 1: return p1[index];
	}
	std::cerr << "Unknown error during Path access. Exiting..." << std::endl;
      	exit(EXIT_FAILURE);	
}	

class Simulation {
	private:
		int size;
		int accepted;
		int steps;
		double delta;
		double eta;
		Paths *p;
		void Metrostep_path(int);
		void Metrostep_switch();
	public:
		Simulation(double, int, double, int);
		~Simulation();
		void Metrostep();
		double Acceptance();
		double Measure_x2();
		int Measure_sigma();
};

Simulation::Simulation(double Neta, int N, double d = 0, int init = 0) {
	size = N;
	eta = Neta/N;
	steps = 0;
	accepted = 0;
	p = new Paths(N, init);
	if (d == 0)
		delta = 2 * sqrt(eta);
	else
		delta = d;
}

double Simulation::Acceptance() {
	double a = accepted;
	a /= steps;
	return a;
}

double Simulation::Measure_x2() {
	int i;
	double m = 0;
	for (i = 0; i < size; i++) {
		m += pow((*p)(0,i), 2);
		m += pow((*p)(1,i), 2);
	}
	m /= 2*size;
	return m;
}

void Simulation::Metrostep_path(int pi) {
	int i;
	double x, yp, r;
	for (i = 0; i < size; i++) {	// spazza il cammino, rispetta il bilancio dettagliato
		yp = (*p)(pi,i) + delta*(2*ran2() - 1);

		r = exp( - (pow(yp,2) - pow((*p)(pi,i),2))*((eta/2) + (1 / eta)) + (1/eta) * (yp - (*p)(pi,i))*((*p)(pi,i+1) + (*p)(pi,i-1)));

		if (r > 1) {
			accepted++;
			(*p)(pi,i) = yp;
		} else {
			x = ran2();
			if (x < r) {
				accepted++;
				(*p)(pi,i) = yp;
			}
		}
		steps++;
	}

}

void Simulation::Metrostep_switch() {
	int j0 = size-1;
	double r = - (1 / eta) * ((*p)(1,j0+1) - (*p)(0,j0+1)) * ((*p)(1,j0) - (*p)(0,j0));
	r = exp(r);
	double x = ran2();
	if (x < r) {
		accepted++;
		p->flip_sigma();
	}
	steps++;
}

void Simulation::Metrostep() {
	Metrostep_path(0);
	Metrostep_path(1);
	Metrostep_switch();
}

int Simulation::Measure_sigma() {
	return p->get_sigma();
}

Simulation::~Simulation() {
	// Destructor dell'oggetto Simulation
	delete p;
}

int main() {
	ran2_init();
	
	std::cout.precision(17); // double precision

	unsigned int n_measures = 1000000;
	unsigned int n_skip = 10;
	unsigned int n_thermal = 100000;
	unsigned int n = 30;
	double neta = 3;
	int init = 1; 	// 0: freddo, 1: caldo
	double delta = 0.5; 	// se passato

	Simulation sim(neta, n, delta, init);

	unsigned int i,j;

	for (i = 0; i < n_thermal; i++)
		sim.Metrostep();
	std::cout << "#Beginning simulation with Neta = " << neta << ", N = " << n << " \n";
	std::cout << "#y^2\tsigma\n";

	for (i = 0; i < n_measures; i++) {
		for (j = 0; j < n_skip; j++)
			sim.Metrostep();
			
		std::cout << sim.Measure_x2() << "\t" << sim.Measure_sigma() << "\n";
	} 
	std::cout << "#Generated " << n_measures << " measures with acceptance: " << sim.Acceptance() << std::endl;
	ran2_save();
	return 0;
}


