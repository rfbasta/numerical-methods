# numerical-methods
Repo privato contenente il codice per i progetti del corso di Metodi Numerici

## Contenuti
- Modulo 1: Modello di Ising 2D e transizioni di fase
- Modulo 3: Path Integral
- Modulo 4: PDE
- Libreria di analisi dati *analysis.py*
	- Medie e varianze (sul campione e sulla media)
	- Algoritmo di Blocking (NumPy-based! 10^6 dati in ~50ms!) per il calcolo degli errori di dati autocorrelati
	- Algoritmo Bootstrap (lento...) per il calcolo degli errori di osservabili generiche
- Generatore di numeri casuali *ran2*, in versione originale (NumRep C, float ran2()) e a doppia precisione (Athena++, Princeton University, double ran2())

## Da fare:
- Analysis.py:
	- Velocizzare Bootstrap sfruttando efficacemente NumPy
		- (Fatto tentativo: fboostrap_wbins. Testare e sistemare i parametri in input) [add: sembra funzionare, fatto un confronto veloce]
	- Tentare calcolo dell'autocorrelazione con np.roll?
- Prendere i parametri in input in modo sensato (*argv*?)
	- fatto per l'ising
- Potenziale anarmonico
- Particella su un cerchio
- Sistema di due fermioni


## Tutorial semplice di git

1. Aggiungi i file cambiati: git add <filename>
2. Commit: git commit -m "Commit message"
3. Push: git push origin master
