import numpy as np
import random as ran
import matplotlib.pyplot as plt

def avg(data):
    s = data.sum()
    s /= np.size(data)
    return s

def sample_var(data):
    if (np.size(data) == 1):
        return -1
    m = avg(data);
    d = (data - m)**2
    d = d.sum()
    d /= (np.size(data) - 1)
    return d

def sample_std(data):
    return np.sqrt(sample_var(data))

def mean_var(data):
    return (sample_var(data) / np.size(data))

def mean_std(data):
    return np.sqrt(mean_var(data))

def halve(data):
    s = data.size
    w = np.copy(data)
    if ((s % 2) != 0):
        w = w[1:]
        s -= 1
    s /= 2
    s = int(s)
    return (w.reshape(s, 2).sum(axis=1) / 2)

        
def blocking(data):
    b = np.array([])
    x = np.copy(data)
    b = np.append(b, mean_std(x))
    for i in range(0, int(np.log2(np.size(data))) - 1):
        x = halve(x)
        b = np.append(b, mean_std(x))
    return b



def sample_blocking(data):
    b = np.array([])
    x = data
    b = np.append(b, sample_std(x))
    for i in range(0, int(np.log2(np.size(data))) - 1):
        x = halve(x)
        b = np.append(b, sample_std(x))
    return b

def plotblocking(data):
    b = blocking(data)
    plt.plot(b)
    plt.xlabel("Block size (log2)")
    plt.ylabel("Standard deviation (naïve estimate)")
    plt.show()
    return


def sample_plotblocking(data):
    b = sample_blocking(data)
    plt.plot(b)
    plt.xlabel("Block size (log2)")
    plt.ylabel("Standard deviation (naïve estimate)")
    plt.show()
    return

def binder(data):
    x4 = data**4
    x2 = data**2
    d = avg(x4) 
    d /= (3 * (avg(x2)**2))
    return d


def binder_bootstrap_wbins(data, blocksize):
    n_samples = 10     # semplifichiamoci la vita
    samples_binder = np.array([])
    for i in range(0, n_samples):
        sample = np.array([])
        print(i)
        for j in range(0, np.size(data) // blocksize):
            x = ran.randrange(0, np.size(data) - blocksize)
            for k in range(0, blocksize):
                sample = np.append(sample, data[x+k])
        samples_binder = np.append(samples_binder, binder(sample))
    binder_std = sample_std(samples_binder)
    return binder_std

def bootstrap_wbins(f, data, blocksize):
    n_samples = 10     # semplifichiamoci la vita
    samples = np.array([])
    for i in range(0, n_samples):
        sample = np.array([])
        print(i)
        for j in range(0, np.size(data) // blocksize):
            x = ran.randrange(0, np.size(data) - blocksize)
            for k in range(0, blocksize):
                sample = np.append(sample, data[x+k])
        samples = np.append(samples, f(sample))
    f_std = sample_std(samples)
    return f_std

def fbootstrap_wbins(f, data, blocksize):
    n_samples = 100     # semplifichiamoci la vita
    x = np.copy(data)
    samples = np.array([])
    ndata = x.size 
    ndiscard = ndata % blocksize
    if (ndiscard != 0):
        x = x[ndiscard:]
    nblocks = int(ndata / blocksize) 
    blocks = x.reshape(nblocks, blocksize)
    for i in range(0, n_samples):
        sample = np.array([])
        print(i)
        for j in range(0, nblocks):
            x = np.random.randint(0, nblocks)
            sample = np.append(sample, blocks[x])
        samples = np.append(samples, f(sample))
    f_std = sample_std(samples)
    return f_std

def autocorrelation2(data):
    C = 0
    m = avg(data)
    d = sample_std(data)
    for k in range(1, np.size(data)//100):
        for i in range(0, (np.size(data) // 100) - k):
            C += (data[i] - m)*(data[i+k] - m) / (d**2 * (np.size(data) - k))
    return C

def autocorrelation_m(data):
    m = avg(data)
    dfx = data - m
    dfx = np.array([dfx])
    matrix = dfx * dfx.T



    for i in range(0, np.size(data)):
        for j in range(0, np.size(data)):
                matrix[i][j] /= (np.size(data) - j + i)
    C = matrix.sum()
    C /= 2
    C /= sample_var(data)
    C -= matrix.trace()
    return C


def thermalize(data, x):  # scraps first x*100% of data
    # sanitize inputs
    if (x > 1):
        x = 1 
    if (x < 0):
        x = 0
    n = np.size(data) * x
    n = round(n)
    d = data[n:]
    return d

def model_y2(neta):
    y = 0.5
    y += 1 / (np.exp(neta) - 1)
    return y
