#include <iostream>
#include <cmath>
#include "../ran2_double.cpp"

// ISING: codice pulito con allocazione statica

const int size = 10;
const int volume = size*size;

int random_spin() { 		// Genera casualmente \pm 1 con ran2
	double x = ran2();
	if (x > 0.5)
		return 1;
	else
		return -1;
}

class Simulation { 			// L'oggetto che gestisce la simulazione
	private:
		int accepted;		// passi accettati
		int steps;		// passi fatti
		double beta;		// parametro beta
		double hfield;	// Campo magnetico esterno
		int lat[size][size];		// Lattice
		int next[size];
		int prev[size];
	public:
		Simulation(double, double, int);	// Costruttore
		void Metrostep();			// Singolo passo Metropolis
		double Acceptance() const;			// Restituisce l'accettanza
		double Energy() const ;			// Misura l'energia
		double Magnetization() const;			// Misura la magnetizzazione
};

Simulation::Simulation(double b, double h, int init = 1) {
	// Costruttore dell'oggetto Simulation
	// Importa tutti i parametri necessari, e costruisce un oggetto Lattice
	hfield = h;
	beta = b;
	accepted = 0;
	steps = 0;
	// prev, next
	for (int i = 0; i < size; i++) {
		next[i] = i+1;
		prev[i] = i-1;
	}
	next[size-1] = 0;
	prev[0] = size-1;
	// init
	switch (init) { 
		case 0:	for (int i = 0; i < size; i++) {
				for (int j = 0; j < size; j++)
					lat[i][j] = 1;	// partenza a freddo
			}
			break;
		case 1: for (int i = 0; i < size; i++) {
				for (int j = 0; j < size; j++)
					lat[i][j] = random_spin(); // partenza a caldo
			}
			break;
		default: // implementare caricamento da un file
			 // per ora, 
			 exit(EXIT_FAILURE);
	}
}

double Simulation::Acceptance() const {
	double a = accepted;
	a /= steps;
	return a;
}

void Simulation::Metrostep() {
	// Scelgo un sito casualmente
	int i = floor((ran2() * size));
	int j = floor((ran2() * size));

	double f = lat[i][next[j]] + lat[i][prev[j]] + lat[next[i]][j] + lat[prev[i]][j];
	f = beta*(f + hfield);

	double r = exp(-2.0 * lat[i][j] * f);

	// Test di Metropolis
	double x = ran2();
	if (x < r) {
		lat[i][j] = -lat[i][j];
		accepted++;	// aggiorno l'accettanza
	}
	steps++;		// aggiorno il numero di passi
}

double Simulation::Energy() const {
	double e = 0;
	double f;
	for (int i = 0; i < size; i++) {
		for (int j = 0; j < size; j++) {
			f = lat[i][next[j]] + lat[i][prev[j]] + lat[next[i]][j] + lat[prev[i]][j];
			e -= 0.5*f*lat[i][j];
			e -= hfield*lat[i][j];	
		}
	}
	e /= volume;
	return e;
}

double Simulation::Magnetization() const {
	double m = 0;
	for (int i = 0; i < size; i++)
		for (int j = 0; j < size; j++) 
			m += lat[i][j];
	m /= volume;
	return m;
}




int main(int argc, char** argv) {

	if (argc != 7) {
		std::cout << "Usage: 6 arguments: n_measures, n_skip, n_thermal; beta, hfield, init" << std::endl;
		return 1;
	}

	// Inizializzo ran2
	ran2_init();
	
	// Stampa i double con la stessa precisione
	// con cui vengono immagazzinati 
	std::cout.precision(17); 
	
	// Parametri. Da prendere come argomenti
	// in modo da non dover ricompilare
	const unsigned int n_measures = atoi(argv[1]);
	const unsigned int n_skip = atoi(argv[2]) * volume; 
	const unsigned int n_thermal = atoi(argv[3]);
	const double beta = atof(argv[4]);
	const double hfield = atof(argv[5]);
	const int init = atoi(argv[6]);	// 0: freddo, 1: caldo
	// Inizializzo la simulazione
	Simulation sim(beta, hfield, init);

	// Termalizzazione
	std::cout << "#Termalizzazione... (salto " << n_thermal << " misure)" << std::endl;
	for (unsigned int i = 0; i < (n_thermal*n_skip); i++)
		sim.Metrostep();

	// Inizio misure
	std::cout << "#Inizio simulazione con beta = " << beta << ", N = " << size << ", h = " << hfield << " \n";
	std::cout << "#Calcolo " << n_measures << " misure, saltando " << n_skip << " * N^2 passi per misura." << std::endl;
	std::cout << "#Iter.\tMagnetiz\tEnergia\n";

	for (unsigned int i = 0; i < n_measures; i++) {
		for (unsigned int j = 0; j < n_skip; j++)
			sim.Metrostep();
		std::cout << i << "\t" << sim.Magnetization() << "\t" << sim.Energy() << "\n";
	} 

	// Accettanza
	std::cout << "#Generate " << n_measures << " misure con accettanza: " << sim.Acceptance() << std::endl;

	// Salvo stato del generatore
	ran2_save(); 
	return EXIT_SUCCESS;
}

