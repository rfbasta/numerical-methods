import numpy as np
import matplotlib.pyplot as plt
from analysis import xfbootstrap_wbins
from tqdm import tqdm


Ns = np.array([20,30,40,50,60])
betas = np.array([0.3  , 0.32 , 0.34 , 0.35 , 0.36 , 0.37 , 0.38 , 0.39 , 0.395,
       0.4  , 0.405, 0.41 , 0.415, 0.42 , 0.425, 0.43 , 0.435, 0.44 ,
       0.445, 0.45 , 0.455, 0.46 , 0.47 , 0.475, 0.48 , 0.485, 0.49 ,
       0.495, 0.5  , 0.505])

n_samples=500

def n2var(x):
    return np.var(x,ddof=1)*(N**2)

chisn = []
dchisn = []
Csn = []
dCsn = []

for N in tqdm(Ns, desc='Total'):
    chis = []
    dchis = []
    Cs = []
    dCs = []
    for i, beta in tqdm(enumerate(betas), desc=f'n = {N}'):
        e, m = np.loadtxt(f'{N}/ising_{beta}', unpack=True)
        trash, te, tm = np.loadtxt(f'auto_time/{N}.dat', unpack=True)
        e = e[10000:]
        m = np.abs(m)[10000:]
        chi = n2var(m)
        C = n2var(e)
        dchi = xfbootstrap_wbins(n2var, m, int(tm[i]), n_samples)
        dC = xfbootstrap_wbins(n2var, e, int(te[i]), n_samples)
        #print(f'{beta}, chi = {chi} +- {dchi}, C = {C} +- {dC}')
        chis.append(chi)
        dchis.append(dchi)
        Cs.append(C)
        dCs.append(dC)
    chisn.append(np.asarray(chis))
    dchisn.append(np.asarray(dchis))
    Csn.append(np.asarray(Cs))
    dCsn.append(np.asarray(dCs))

chisn = np.asarray(chisn)
dchisn = np.asarray(dchisn)
Csn = np.asarray(Csn)
dCsn = np.asarray(dCsn)

for i, N in enumerate(Ns):
    np.savetxt(f'bs_data/{N}.dat', np.column_stack([betas, chisn[i], dchisn[i], Csn[i], dCsn[i]]), header="beta\tchi\tdchi\tC\tdC")

print("Data saved")

