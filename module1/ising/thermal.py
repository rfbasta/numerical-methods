#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 24 20:05:52 2020

@author: rfrancesco
"""
import matplotlib.pyplot as plt
import numpy as np
from analysis import plot_therm_avg, error_therm_avg
from tqdm import tqdm

Ns = np.array([20,30,40,50,60])
betas = np.array([0.30,0.36,0.395,0.415,0.435,0.455,0.49,0.32,0.37,0.40,0.42,0.44,0.46,0.50,0.34,0.38,0.405,0.425,0.445,0.47,0.35,0.39,0.41, 0.43, 0.45,0.48])
betas.sort()


for N in tqdm(Ns[::-1]):
    for beta in tqdm(betas[::-1]):
        e, m = np.loadtxt('%d/ising_%s' % (N, beta), unpack=True)
        print(error_therm_avg(e), error_therm_avg(np.abs(m)))
    

