#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 24 20:05:52 2020

@author: rfrancesco
"""
import matplotlib.pyplot as plt
import numpy as np
from analysis import blocking, r_measurement
from tqdm import tqdm

Ns = np.array([20,30,40,50,60])
betas = np.array([0.30,0.36,0.395,0.415,0.435,0.455,0.49,0.32,0.37,0.40,0.42,0.44,0.46,0.50,0.34,0.38,0.405,0.425,0.445,0.47,0.35,0.39,0.41, 0.43, 0.45,0.48])
betas.sort()


plt.rcParams['figure.figsize'] = [10, 10]



for N in Ns:
    print(f"Analyzing data for N = {N}")
    trash, tes, tms = np.loadtxt(f"auto_time/{N}.dat", unpack=True)
    eraws = []
    mraws = []
    eblocks = []
    mblocks = []
    es = []
    ms = []
    des = []
    dms = []

    for beta in tqdm(betas):
        eraw, mraw = np.loadtxt('%d/ising_%s' % (N, beta), unpack=True)
        eraw = eraw[10000:]
        mraw = mraw[10000:]
        mraw = np.abs(mraw)
        a = blocking(eraw)
        b = blocking(mraw)
        eraws.append(eraw)
        mraws.append(mraw)
        eblocks.append(a)
        mblocks.append(b)
    
    for i, beta in enumerate(betas):
        e = eraws[i].mean()
        m = mraws[i].mean()
        plt.subplot(2,1,1)
        plt.title('b = %s, E = %.6f, M = %.6f' % (beta, e, m))
        plt.ylabel('dE')
        plt.grid(axis='y')
        plt.yticks(np.linspace(eblocks[i].min(), eblocks[i].max(), 10))
        plt.plot(eblocks[i])
        plt.subplot(2,1,2)
        plt.ylabel('dM')
        plt.grid(axis='y')
        plt.yticks(np.linspace(mblocks[i].min(), mblocks[i].max(), 10))
        plt.plot(mblocks[i])
        plt.show(block=False)
        print(f"(N, beta) = ({N}, {beta})")
        print(f"te ~ {tes[i]}: dE ~ {eblocks[i][0]*np.sqrt(1 + 2*tes[i])}") 
        print(f"tm ~ {tms[i]}: dM ~ {mblocks[i][0]*np.sqrt(1 + 2*tms[i])}")
        confirmed = False
        while not confirmed:
            e = eraws[i].mean()
            m = mraws[i].mean()
            de = (float(input("dE? ")))
            dm = (float(input("dM? ")))
            e, de = r_measurement(e, de)
            m, dm = r_measurement(m, dm)
            print(f"{beta}: E = {e} +- {de}, M = {m} +- {dm}")
            answer = ' '
            while ((answer != 'y') and (answer != 'n')):
                    answer = input('Ok? (y/n) ')
            if (answer == 'y'):
                confirmed = True
        es.append(e)
        ms.append(m)
        des.append(de)
        dms.append(dm)
        print(f"Saved {beta}: E = {e} +- {de}, M = {m} +- {dm}")
        plt.close()
    np.savetxt(f"blocked/{N}.dat", np.column_stack([betas, es, des, ms, dms]), header="beta\te\tde\tm\tdm\t")
    print(f"All data for {N} was successfully saved.")


plt.show()

