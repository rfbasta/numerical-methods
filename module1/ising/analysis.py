import numpy as np
import random as ran
import matplotlib.pyplot as plt
from tqdm import tqdm

def sample_var(data):
    d = data.var(ddof=1)
    return d

def sample_std(data):
    return np.sqrt(sample_var(data))

def mean_var(data):
    return (sample_var(data) / np.size(data))

def mean_std(data):
    return np.sqrt(mean_var(data))

def halve(data):
    s = data.size
    w = np.copy(data)
    if ((s % 2) != 0):
        w = w[1:]
        s -= 1
    s /= 2
    s = int(s)
    return (w.reshape(s, 2).sum(axis=1) / 2)

        
def blocking(data):
    b = []
    x = np.copy(data)
    b.append(mean_std(x))
    for i in range(0, int(np.log2(np.size(data))) - 1):
        x = halve(x)
        b.append(mean_std(x))
    return np.asarray(b)



def sample_blocking(data):
    b = np.array([])
    x = data
    b = np.append(b, sample_std(x))
    for i in range(0, int(np.log2(np.size(data))) - 1):
        x = halve(x)
        b = np.append(b, sample_std(x))
    return b

def plotblocking(data):
    b = blocking(data)
    plt.plot(b)
    plt.xlabel("Block size (log2)")
    plt.ylabel("Standard deviation (naïve estimate)")
    plt.show()
    return


def sample_plotblocking(data):
    b = sample_blocking(data)
    plt.plot(b)
    plt.xlabel("Block size (log2)")
    plt.ylabel("Standard deviation (naïve estimate)")
    plt.show()
    return

def binder(data):
    x4 = data**4
    x2 = data**2
    d = x4.mean() 
    d /= (3 * (x2.mean()**2))
    return d


def binder_bootstrap_wbins(data, blocksize):
    n_samples = 10     # semplifichiamoci la vita
    samples_binder = np.array([])
    for i in range(0, n_samples):
        sample = np.array([])
        print(i)
        for j in range(0, np.size(data) // blocksize):
            x = ran.randrange(0, np.size(data) - blocksize)
            for k in range(0, blocksize):
                sample = np.append(sample, data[x+k])
        samples_binder = np.append(samples_binder, binder(sample))
    binder_std = sample_std(samples_binder)
    return binder_std

def bootstrap_wbins(f, data, blocksize):
    n_samples = 10     # semplifichiamoci la vita
    samples = np.array([])
    for i in range(0, n_samples):
        sample = np.array([])
        print(i)
        for j in range(0, np.size(data) // blocksize):
            x = ran.randrange(0, np.size(data) - blocksize)
            for k in range(0, blocksize):
                sample = np.append(sample, data[x+k])
        samples = np.append(samples, f(sample))
    f_std = sample_std(samples)
    return f_std

def fbootstrap_wbins(f, data, blocksize, n_samples):
    x = np.copy(data)
    samples = np.array([])
    ndata = x.size 
    ndiscard = ndata % blocksize
    if (ndiscard != 0):
        x = x[ndiscard:]
    nblocks = int(ndata / blocksize) 
    blocks = x.reshape(nblocks, blocksize)
    for i in range(0, n_samples):
        sample = np.array([])
        print(i)
        for j in range(0, nblocks):
            x = np.random.randint(0, nblocks)
            sample = np.append(sample, blocks[x])
        samples = np.append(samples, f(sample))
    f_std = sample_std(samples)
    return f_std

def optfbootstrap_wbins(f, data, blocksize, n_samples):
    x = np.copy(data)
    samples = []
    ndata = x.size 
    ndiscard = ndata % blocksize
    if (ndiscard != 0):
        x = x[ndiscard:]
    nblocks = int(ndata // blocksize) 
    blocks = x.reshape(nblocks, blocksize)
    for i in tqdm(range(0, n_samples)):
        sample = []
        for j in range(0, nblocks):
            x = np.random.randint(0, nblocks)
            sample.append(blocks[x])
        samples.append(f(sample))
    f_std = np.std(samples, ddof=1)
    return f_std

def xfbootstrap_wbins(f, data, blocksize, n_samples):
    # extra-fast bootstrap w numpy
    x = np.copy(data)
    ndata = x.size 
    ndiscard = ndata % blocksize
    if (ndiscard != 0):
        x = x[ndiscard:]
    nblocks = int(ndata // blocksize) 
    blocks = x.reshape(nblocks, blocksize)
    idx = np.random.randint(0, nblocks, size=(n_samples, nblocks))
    samples = np.apply_along_axis(f, 1, blocks[idx])
    f_std = np.std(samples, ddof=1)
    return f_std



def autocorr(x):
    tmp = x.copy()
    tmp = tmp - tmp.mean()
    aut = np.correlate(tmp, tmp, mode='full')
    aut = aut[aut.size//2:] / aut.max()
    return aut

def tint(x):
    return np.cumsum(autocorr(x))

def plot_tint(x):
    plt.plot(tint(x))
    plt.show()
    
def model_y2(neta):
    y = 0.5
    y += 1 / (np.exp(neta) - 1)
    return y

def plot_therm_avg(x):
    avgs = []
    for i in range(0, 10000):
        avgs.append(x[i:].mean())
    plt.plot(avgs)
    plt.show()

def error_therm_avg(x):
    avgs = []
    for i in range(0, 10000):
        avgs.append(x[i:].mean())
    avgs = np.asarray(avgs)
    return np.abs(avgs.max() - avgs.min())/2

def rt1(x): #round to 1st significant digit
    return round(x, -int(np.floor(np.log10(abs(x)))))

def rte(x,dx):  # round error to measurement's 1st significant digit
    return round(x, -int(np.floor(np.log10(abs(dx)))))

def r_measurement(x,dx):
    return (rte(x,dx), rt1(dx))
