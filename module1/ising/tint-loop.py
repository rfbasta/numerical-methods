#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 24 20:05:52 2020

@author: rfrancesco
"""
import matplotlib.pyplot as plt
import numpy as np
from analysis import tint, autocorr
from tqdm import tqdm

Ns = np.array([20,30,40,50,60])
betas = np.array([0.30,0.36,0.395,0.415,0.435,0.455,0.49,0.32,0.37,0.40,0.42,0.44,0.46,0.50,0.34,0.38,0.405,0.425,0.445,0.47,0.35,0.39,0.41, 0.43, 0.45,0.48])
betas.sort()

for N in Ns:
    print(f"Analyzing data for N = {N}")
    tes = []
    tms= [] 
    for beta in tqdm(betas):
        eraw, mraw = np.loadtxt('%d/ising_%s' % (N, beta), unpack=True)
        eraw = eraw[10000:]
        mraw = mraw[10000:]
        mraw = np.abs(mraw)
        e = eraw.mean()
        m = mraw.mean()
        a = tint(eraw)
        b = tint(mraw)
        plt.subplot(2,1,1)
        plt.title('b = %s, E = %.6f, M = %.6f' % (beta, e, m))
        plt.ylabel('te')
        plt.xlim(0,2000)
        plt.plot(a)
        plt.subplot(2,1,2)
        plt.ylabel('tm')
        plt.xlim(0,2000)
        plt.plot(b)
        plt.show(block=False)
        print(f"(N, beta) = ({N}, {beta})")
        aest = int(round(a[1:1000].max()))
        best = int(round(b[1:1000].max()))
        if (input(f"te = {aest}, tm = {best}? (y/n)") != 'y'):
            te = float(input("texp_e? "))
            tm = float(input("texp_m? "))
        else:
            te = aest
            tm = best
        tes.append(te)
        tms.append(tm)
        print(f"Saved: {beta}: te= {te}, tm= {tm}")
        plt.close()
    np.savetxt(f"auto_time/{N}.dat", np.column_stack([beta, tes, tms]), header="beta\tte\ttm\t")
    print(f"All data for {N} was successfully saved.")
plt.show()

