#!/bin/bash

parallel -j 2 --progress --results "out/ising_{1}" ./xising 100000 100 20 {1} 0 1 ::: 0.30 0.32 0.34 0.36 0.38 0.40 0.42 0.44 0.46 0.48
