import numpy as np
import matplotlib.pyplot as plt
from tqdm import tqdm

Ns = [20,30,40,50,60]
beta = []
E = []
dE = []
M = []
dM = []

for N in tqdm(Ns):
    bb, ee, dee, mm, dmm = np.loadtxt('blocked/%s.dat' % N, unpack=True)
    beta.append(bb)
    E.append(ee)
    dE.append(dee)
    M.append(mm)
    dM.append(dmm)

plt.figure(1)
plt.title('Densità di energia')
plt.xlabel('$\\beta$')
plt.ylabel('E/N^2')
for i, N in enumerate(Ns):
    plt.errorbar(beta[i], E[i], dE[i], label='N = %s' % N, linestyle=' ', marker='+')
plt.legend()

plt.figure(2)
plt.title('Magnetizzazione')
plt.xlabel('$\\beta$')
plt.ylabel('M/N^2')
for i, N in enumerate(Ns):
    plt.errorbar(beta[i], M[i], dM[i], label='N = %s' % N, linestyle=' ', marker='+')
plt.legend()

plt.show()
