#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 24 20:05:52 2020

@author: rfrancesco
"""
import matplotlib.pyplot as plt
import numpy as np
from sys import argv
from analysis import tint, autocorr
from tqdm import tqdm

Ns = np.array([20,30,40,50,60])
betas = np.array([0.30,0.36,0.395,0.415,0.435,0.455,0.49,0.32,0.37,0.40,0.42,0.44,0.46,0.50,0.34,0.38,0.405,0.425,0.445,0.47,0.35,0.39,0.41, 0.43, 0.45,0.48])
betas.sort()

N = argv[1]
beta = argv[2]

eraw, mraw = np.loadtxt('%s/ising_%s' % (N, beta), unpack=True)
eraw = eraw[10000:]
mraw = mraw[10000:]
mraw = np.abs(mraw)
plt.subplot(2,1,1)
plt.title('b = %s' % (beta))
plt.xlim(0,1000)
plt.ylabel('dE')
plt.plot(tint(eraw))
plt.subplot(2,1,2)
plt.xlim(0,1000)
plt.ylabel('dM')
plt.plot(tint(mraw))
plt.show()

