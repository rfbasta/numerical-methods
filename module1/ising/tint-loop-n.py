#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 24 20:05:52 2020

@author: rfrancesco
"""
import matplotlib.pyplot as plt
import numpy as np
from analysis import tint, autocorr
from tqdm import tqdm

Ns = np.array([20,30,40,50,60])
betas = np.array([0.30,0.36,0.395,0.415,0.435,0.455,0.49,0.32,0.37,0.40,0.42,0.44,0.46,0.50,0.34,0.38,0.405,0.425,0.445,0.47,0.35,0.39,0.41, 0.43, 0.45,0.48])
betas.sort()


plt.rcParams['figure.figsize'] = [10, 10]
plt.grid(b=True, which='both', axis='y')



for N in Ns:
    print(f"Analyzing data for N = {N}")
    tes = []
    tms = []
    eraws = []
    mraws = []
    etints = []
    mtints = []

    for beta in tqdm(betas):
        eraw, mraw = np.loadtxt('%d/ising_%s' % (N, beta), unpack=True)
        eraw = eraw[10000:]
        mraw = mraw[10000:]
        mraw = np.abs(mraw)
        a = tint(eraw)
        b = tint(mraw)
        eraws.append(eraw)
        mraws.append(mraw)
        etints.append(a)
        mtints.append(b)
        tes.append(int(round(a[1:1000].max())))
        tms.append(int(round(b[1:1000].max())))
    
    for i, beta in tqdm(enumerate(betas)):
        plt.subplot(2,1,1)
        plt.title(f'b = {beta}')
        plt.ylabel('te')
        plt.xlim(0,2000)
        plt.plot(etints[i])
        plt.subplot(2,1,2)
        plt.ylabel('tm')
        plt.xlim(0,2000)
        plt.plot(mtints[i])
        plt.show(block=False)
        print(f"(N, beta) = ({N}, {beta})")
        if (input(f"te = {tes[i]}, tm = {tms[i]}? (nothing/n)") == 'n'):
            tes[i] = int(round(input("texp_e? ")))
            tms[i] = int(round(input("texp_m? ")))
        print(f"Saved: {beta}: te= {tes[i]}, tm= {tms[i]}")
        plt.close()
    np.savetxt(f"auto_time/{N}.dat", np.column_stack([betas, tes, tms]), header="beta\tte\ttm\t")
    print(f"All data for {N} was successfully saved.")


plt.show()

