#include <iostream>
#include <cmath>

/* Integrazione di un sistema 1D con il metodo di Lax */


class Array {

	private:
		double * m;
		int n;
	public:
		Array(int);
		~Array();
		double &operator()(int);
};

Array::Array(int n) {
	m = new double[2*n+1];
	this->n = n;
}

Array::~Array() {
	delete [] m;
}

double& Array::operator()(int i) {
	return m[i+n];
}


int main() {
	
	// Precisione in output = double
	std::cout.precision(17);

	// Parametri di integrazione
	const int n = 100;
	const double v = -1;
	const double dt = 0.001;
	const double dx = 0.1;
	const int ninner = 1;
	const double alpha = v*dt/dx;

	// Creazione delle strutture dati
	Array uold(n);
	Array unew(n);

	// Inizializzazione con le condizioni iniziali
	double x = 0;
	for (int i = -n; i <= n; i++) {
		x = i*dx;
		uold(i) = exp(-x*x/2) * cos(x * 6.3);	// Condizioni iniziali
		unew(i) = 0;				// Azzeramento della matrice
	}

	// Integrazione con il metodo di Lax
	for (int itime = 0; itime < 100; itime++) {
		for (int inner = 0; inner < ninner; inner++) {
			for (int i = (-n+1); i <= (n-1); i++)
				unew(i) = 0.5 * (uold(i+1)*(1-alpha) + uold(i-1)*(1+alpha));
			for (int i = -n; i <= n; i++)
				uold(i) = unew(i);
		}
		// Stampa dello stato corrente
		for (int i = -n; i <= n; i++)
			std::cout << i*dx << "\t" << itime*dt*ninner << "\t" << uold(i) << std::endl;
		std::cout << std::endl;	// Riga vuota per GNUPLOT

	}
	return 0;

}
