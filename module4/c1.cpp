#include <iostream>
#include <cmath>

class Array {

	private:
		double * m;
		int n;
	public:
		Array(int);
		~Array();
		double &operator()(int);
};

Array::Array(int n) {
	m = new double[n];
	this->n = n;
}

Array::~Array() {
	delete [] m;
}

double& Array::operator()(int i) {
	while (i < 0)
		i += n;
	while(i >= n)
		i -= n;
	return m[i];
}


int main() {
	
	// Precisione in output = double
	std::cout.precision(17);

	// Parametri di integrazione
	const int n = 1000;
	const double v = 1;
	const double dt = 0.00001;
	const double dx = 0.01;
	const int ninner = 1;
	const double alpha = v*dt/pow(dx,2); // ATTENTO ALLA SCELTA!

	// Creazione delle strutture dati
	Array uold(n);
	Array unew(n);

	// Inizializzazione con le condizioni iniziali
	const double x0 = 5;
	const double sigma = 1;
	double x = 0;
	for (int i = 0; i < n; i++) {
		x = i*dx;
		uold(i) = exp(-pow((x-x0), 2)/(2*sigma));	// Condizioni iniziali
		unew(i) = 0;				// Azzeramento della matrice
	}

/*	// Integrazione 
	for (int itime = 0; itime < 1000; itime++) {
		for (int inner = 0; inner < ninner; inner++) {
			for (int i = 0; i < n; i++)
				unew(i) = uold(i) + alpha * (uold(i-1) - 2*uold(i) + uold(i+1));
			// Condizioni al bordo zero al bordo
			//unew(-n) = 0;
			//unew(n) = 0;
			// Condizioni al bordo derivata zero
			for (int i = 0; i < n; i++)
				uold(i) = unew(i);
		}
		// Stampa dello stato corrente
		for (int i = 0; i < n; i++)
			std::cout << i*dx << "\t" << itime*dt*ninner << "\t" << uold(i) << std::endl;
		std::cout << std::endl;	// Riga vuota per GNUPLOT

	}*/

	for (int i = 0; i < n; i++)
		std::cout << i*dx << "\t" << uold(i) << std::endl;
	return 0;

}
