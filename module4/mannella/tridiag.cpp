#include <iostream>


int main() {
	const int N = 5;

	double* dia = new double[N];
	double* dup = new double[N];
	double* dlo = new double[N];

	double* x = new double[N];
	double* b = new double[N];

	for (int i = 0; i < N; i++) {
		dia[i] = 1;
		dup[i] = 0.5;
		dlo[i] = 0.5;
		b[i] = 2;
	}
	b[0] = 1.5;
	b[4] = 1.5;
	

	double f;
	for (int i = 0; i < (N-1); i++) {
		f = dlo[i] / dia[i];
		dlo[i] = 0;
		dia[i+1] -= f*dup[i];
	        b[i+1] -= f*b[i];
	}



	x[N-1] = b[N-1] / dia[N-1];
	for (int i = (N-2); i >= 0; i--) {
		x[i] = (b[i]  - dup[i] * x[i+1]) / dia[i]; 
	}

	for (int i = 0; i < N; i++)
		std::cout << x[i] << "\t";
	std::cout << std::endl;

	return 0;
}
