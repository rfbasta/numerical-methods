#define _USE_MATH_DEFINES
#include <iostream>
#include <cmath>
#include <cassert>
// Soluzione dell'equazione del trasporto
// Runge-Kutta
// FUNZIONA

class Array {

	private:
		double * m;
	public:
		int n;
		Array(int);
		~Array();
		double &operator()(int);
};

Array::Array(int n) {
	m = new double[n];
	this->n = n;
}

Array::~Array() {
	delete [] m;
}

double& Array::operator()(int i) {
	while (i < 0)
		i += n;
	while(i >= n)
		i -= n;
	return m[i];
}

double mod(double x, double xp) {
	while (x < 0)
		x += xp;
	while (x >= xp)
		x -= xp;
	return x;
}

double gauss(double x,double x0, double s) {
	return exp(-pow(x-x0,2)/(2*pow(s,2)));
}

void d2x(Array& u, Array& dest, double dx) {
	for (int i = 0; i < u.n; i++)
		dest(i) = (u(i+1) - u(i-1))/(2*dx);
}

void copy(Array& source, Array& dest) {
	assert(source.n == dest.n);
	for (int i = 0; i < source.n; i++)
		dest(i) = source(i);
}


int main() {
	
	// Precisione in output = double
	std::cout.precision(17);

	// Parametri di integrazione
	const int n = 1000;
	const double v = 1;
	const double dt = 0.00001;
	const double dx = 0.01;
	const double L = n*dx;
	const int nitime = 1000000;
	const double alpha = v*dt/(2*dx); // ATTENTO ALLA SCELTA!
	const int RK = 2;
	// Creazione delle strutture dati
	Array uold(n);
	Array unew(n);
	Array temp(n);
	Array u(n);

	// Inizializzazione con le condizioni iniziali
	//const double k = 2*M_PI*2/L;
	const double sigma = 0.5;
	const double x0 = 5;
	double x = 0;
	for (int i = 0; i < n; i++) {
		x = i*dx;
		uold(i) = gauss(x,x0,sigma);;
		unew(i) = 0;				// Azzeramento della matrice
	}
	double t = 0;
	const double t_measure = 0.25; //1.52;
	double tau = 1 + t_measure;

	// Integrazione 
	for (int itime = 0; itime < nitime; itime++) {
		if (tau >= t_measure) {
			// Stampa dello stato corrente
			for (int i = 0; i < n; i++)
				std::cout << i*dx << "\t" << itime*dt << "\t" << uold(i) << "\t" << gauss(mod(i*dx - v*itime*dt,L),x0,sigma) << std::endl;
			std::cout << std::endl;	// Riga vuota per GNUPLOT
			tau = 0;
		}
		// Runge-Kutta	
		copy(uold, u);
		for (int k = RK; k > 0; --k) {
			d2x(u, temp, dx);
			for (int i = 0; i < n; i++) 
				u(i) = uold(i) - v*dt * temp(i) / k;
		}
		// Ricopio la matrice nuova
		copy(u, uold);
		tau += dt;	

	}

	return 0;

}
