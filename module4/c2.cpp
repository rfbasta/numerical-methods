#define _USE_MATH_DEFINES
#include <iostream>
#include <cmath>


class Array {

	private:
		double * m;
		int n;
	public:
		Array(int);
		~Array();
		double &operator()(int);
};

Array::Array(int n) {
	m = new double[n];
	this->n = n;
}

Array::~Array() {
	delete [] m;
}

double& Array::operator()(int i) {
	while (i < 0)
		i += n;
	while(i >= n)
		i -= n;
	return m[i];
}


int main() {
	
	// Precisione in output = double
	std::cout.precision(17);

	// Parametri di integrazione
	const int n = 1000;
	const double v = 1;
	const double dt = 0.00001;
	const double dx = 0.01;
	const double L = n*dx;
	const int nitime = 1000000;
	const int ninner = 20000;
	const double alpha = v*dt/pow(dx,2); // ATTENTO ALLA SCELTA!

	// Creazione delle strutture dati
	Array uold(n);
	Array unew(n);

	// Inizializzazione con le condizioni iniziali
	const double k = 2*M_PI*2/L;
	double x = 0;
	for (int i = 0; i < n; i++) {
		x = i*dx;
		uold(i) = sin(k*x);
		unew(i) = 0;				// Azzeramento della matrice
	}
	double t = 0;
	const double t_measure = 1.52;
	double tau = 1 + t_measure;

	// Integrazione 
	for (int itime = 0; itime < nitime; itime++) {
		if (tau >= t_measure) {
			// Stampa dello stato corrente
			for (int i = 0; i < n; i++)
				std::cout << i*dx << "\t" << uold(i) << "\t" << sin(k*i*dx)*exp(-pow(k,2)*v*itime*dt) << std::endl;
			std::cout << std::endl;	// Riga vuota per GNUPLOT
			tau = 0;
		}
		for (int i = 0; i < n; i++)
				unew(i) = uold(i) + alpha * (uold(i-1) - 2*uold(i) + uold(i+1));
		// Ricopio la matrice nuova
		for (int i = 0; i < n; i++)
				uold(i) = unew(i);
		tau += dt;	

	}

	return 0;

}
