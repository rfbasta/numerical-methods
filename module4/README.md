# PDE

- lax1.cpp: metodo (non ben funzionante, solo prim'ordine) di lax. 
- c1.cpp: Definisce una griglia e stampa le condizioni iniziali per plot su gnuplot
- c2.cpp: Integra, a tre tempi diversi, l'equazione \partial_t u = \nu \partial^2_x u (FUNZIONANTE!)
- c4.cpp: Integra l'equazione 1D del trasporto, discretizzando in avanti la derivata temporale e in modo simmetrico la derivata spaziale. (FUNZIONANTE)
	- c4-2.cpp: utilizza funzioni copy(s,d) e d2x(s,d). Decisamente brutte ma per ora funzionano. 
- c5.cpp: Integra l'equazione 1D del trasporto, usando un array temporaneo, con il metodo di Runge-Kutta (FUNZIONA)


## Tutorial GNUPLOT:

- Per fare plot semplici, plot "filename"
- Per plottare più cose, replot "filename" ...
- Per specificare quali colonne del file usare, using 1:2 ... (es: x y //: using 1:2; x // y: using 1:3 ...)
- Per plot 2d (formato: x,t,u(x,t), con riga vuota fra due slice diverse)
